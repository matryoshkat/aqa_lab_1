import org.junit.*;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

public class CalculatorTest {

    private Calculator instance;

    @Before
    public void setUp() throws Exception {
        instance = new Calculator();
    }

    @After
    public void tearDown() throws Exception {
        instance = null;
    }

    @Test
    public void addition_2_plus_2_eq_4() {
        double a;
        double b;

        a = b = 2;

        double result = instance.add(a, b);

        Assert.assertEquals(4, result, 0);
    }

    @Test
    public void subtract_5_from_10_is_5() {
        double a = 10;
        double b = 5;

        var result = instance.subtract(a, b);

        Assert.assertEquals(5, result, 0);
    }

    @Test
    public void multiplication_2_by_5_is_10() {
        double a = 2;
        double b = 5;

        var result = instance.multiple(a, b);

        Assert.assertEquals(10, result, 0);
    }

    @Test
    public void division_10_by_5_is_2() {
        double a = 10;
        double b = 5;

        var result = instance.divide(a, b);

        Assert.assertEquals(2, result, 0);
    }

    @Test
    public void division_by_0_is_exception() {
        double a = 2;

        try {
            var result = instance.divide(a, 0);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Divide by 0"));
        }
    }

    @Test
    public void squareRoot_of_4_is_2() {
        double a = 4;

        var result = instance.squareRoot(a);

        Assert.assertEquals(2, result, 0);
    }

    @Test
    public void squareRoot_of_negative_is_exception() {
        double a = -10;
        try {
            instance.squareRoot(a);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Square root of negative number"));
        }
    }

    @Test(timeout = 500)
    public void three_squared_is_9() {
        double a = 3;

        var result = instance.square(a);

        Assert.assertEquals(9, result, 0);
    }

    @Ignore
    @Test
    public void ignored_test() {
        assertTrue(false);
    }
}