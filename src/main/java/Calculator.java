public class Calculator {
    public double add(double a, double b) {
        return a + b;
    }

    public double subtract(double a, double b) {
        return a - b;
    }

    public double multiple(double a, double b) {
        return a * b;
    }

    public double divide(double a, double b) {
        if (b == 0){
            throw new IllegalArgumentException("Divide by 0");
        }
        return a / b;
    }

    public double squareRoot(double a) {
        if (a < 0){
            throw new IllegalArgumentException("Square root of negative number");
        }

        return Math.sqrt(a);
    }

    public double square(double a) {
        return Math.pow(a, 2);
    }
}
